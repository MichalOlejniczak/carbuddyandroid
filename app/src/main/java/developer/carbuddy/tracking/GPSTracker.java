package developer.carbuddy.tracking;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import developer.carbuddy.R;
import developer.carbuddy.models.ObjectOfInterest;
import developer.carbuddy.models.Wallet;

public class GPSTracker extends Service implements LocationListener {

    private NotificationCompat.Builder builder;
    private NotificationManager notificationManager;
    private Notification notification;
    private final Context context;
    private Location location;
    private Wallet wallet;
    private ObjectOfInterest car;
    private double latitude;
    private double longitude;
    private boolean isGPSEnabled = false;
    private boolean isNetworkEnabled = false;
    private boolean canGetLocation = false;
    private LocationManager locationManager;

    private static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = 1;
    private static final long MIN_TIME_BW_UPDATES = 1000;
    private static final int PROXIMITY_NOTIFICATION = 44;

    public GPSTracker(Context context) {
        this.context = context;
        getLocation();
    }

    public GPSTracker(Context context, Wallet wallet, ObjectOfInterest car) {
        this(context);
        this.car = car;
        this.wallet = wallet;
        createNotification();
    }

    public Location getLocation() {
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnabled && !isNetworkEnabled
                && ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_DENIED
                && ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_DENIED) {

        } else {
            this.canGetLocation = true;
            if (isNetworkEnabled) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                Log.d("Network", "Network");
                if (locationManager != null) {
                    location = locationManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                    }
                }
            }
            if (isGPSEnabled) {
                if (location == null) {
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("GPS Enabled", "GPS Enabled");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
            }
        }
        return location;
    }

    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    public void createNotification() {
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        builder = new NotificationCompat.Builder(context);
        builder.setContentTitle(context.getString(R.string.notification_no_wallet));
        builder.setSmallIcon(android.R.mipmap.sym_def_app_icon);
        builder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
        notification = builder.build();
    }

    public void stopUsingGPS() {
        if (locationManager != null &&
                ContextCompat.checkSelfPermission(context,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_DENIED
                && ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_DENIED) {
            locationManager.removeUpdates(GPSTracker.this);
            this.locationManager = null;
            if (notificationManager != null) notificationManager.cancel(PROXIMITY_NOTIFICATION);
            stopSelf();
        }
    }

    public double getLatitude() {
        if (location != null) {
            latitude = location.getLatitude();
        }
        return latitude;
    }

    public double getLongitude() {
        if (location != null) {
            longitude = location.getLongitude();
        }
        return longitude;
    }

    public Location getCurrentLocation() {
        return location;
    }

    @Override
    public void onLocationChanged(Location location) {
        float[] results = new float[3];
        float[] results2 = new float[3];
        if (wallet != null && car != null) {
            Location.distanceBetween(wallet.getLatitude(), wallet.getLongitude(),
                    car.getLatitude(), car.getLongitude(), results);
            Location.distanceBetween(location.getLatitude(), location.getLongitude(),
                    car.getLatitude(), car.getLongitude(), results2);
            if (!wallet.isWithUser() && results[0] > 2.5f && results2[0] < 2.5f) {
                notificationManager.notify(PROXIMITY_NOTIFICATION, notification);
            }
        }
    }

    private double distance(double lat1, double lng1, double lat2, double lng2) {

        double earthRadius = 6371000;

        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);

        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);

        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        double dist = earthRadius * c;

        return dist;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
