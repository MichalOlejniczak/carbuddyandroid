package developer.carbuddy.activities;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;

import butterknife.BindView;
import developer.carbuddy.R;
import developer.carbuddy.waveview.Typefaces;
import developer.carbuddy.waveview.Wave;
import developer.carbuddy.waveview.WaveTextView;

public class SplashActivity extends BaseActivity {
    @BindView(R.id.wave_ale_tv)
    WaveTextView waveAleTv;
    @BindView(R.id.wave_author)
    WaveTextView waveAuthor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initIntro();
    }

    @Override
    protected int onActivityContentView() {
        return R.layout.activity_splash;
    }

    @Override
    protected int getFragmentContainer() {
        return 0;
    }

    @Override
    protected boolean onActivityBackPressed() {
        return false;
    }

    private void initIntro() {
        waveAleTv.setTypeface(Typefaces.get(this, "Comfortaa-Regular.ttf"));
        waveAleTv.setWaveImage(ContextCompat.getDrawable(this, R.mipmap.wave_android_green));

        waveAuthor.setTypeface(Typefaces.get(this, "Comfortaa-Light.ttf"));
        waveAuthor.setWaveImage(ContextCompat.getDrawable(this, R.mipmap.wave_android_green));

        Wave waveAppName = new Wave();
        waveAppName.start(waveAleTv);

        Wave waveAuthor = new Wave();
        this.waveAuthor.postDelayed(new Runnable() {
            @Override
            public void run() {
                runActivity(MainActivity.class);
            }
        }, Wave.TIME_WAVE_ANIMATION + 400);

        waveAuthor.start(this.waveAuthor);
    }
}