package developer.carbuddy.activities;

import android.os.Bundle;
import developer.carbuddy.R;
import developer.carbuddy.fragments.PositionsFragment;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showStartFragment();
    }

    @Override
    protected int onActivityContentView() {
        return R.layout.activity_main;
    }

    @Override
    protected int getFragmentContainer() {
        return R.id.fragment_container_main;
    }

    @Override
    protected boolean onActivityBackPressed() {
        return true;
    }

    void showStartFragment() {
        PositionsFragment positionsFragment = PositionsFragment.getInstance();
        replaceFragment(positionsFragment, positionsFragment.getFragmentTag());
    }
}