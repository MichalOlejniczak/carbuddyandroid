package developer.carbuddy.models;

import android.location.Location;

public class ObjectOfInterest {
    private double longitude;
    private double latitude;

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatLong(Location location) {
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
    }
}
