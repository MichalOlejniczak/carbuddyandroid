package developer.carbuddy.models;


public class Wallet extends ObjectOfInterest {
    private boolean isWithUser = false;

    public Wallet() {
    }

    public boolean isWithUser() {
        return isWithUser;
    }

    public void setWithUser(boolean withUser) {
        isWithUser = withUser;
    }

}
