package developer.carbuddy.fragments;


import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import developer.carbuddy.R;
import developer.carbuddy.adapters.DataBaseAdapter;
import developer.carbuddy.models.ObjectOfInterest;
import developer.carbuddy.models.Wallet;
import developer.carbuddy.tracking.GPSTracker;


public class PositionsFragment extends BaseFragment {
    @BindView(R.id.wallet_checkbox)
    CheckBox walletCheckbox;
    @BindView(R.id.wallet_latitude_layout)
    TextInputLayout walletLatitudeLayout;
    @BindView(R.id.wallet_longitude_layout)
    TextInputLayout walletLongitudeLayout;
    @BindView(R.id.wallet_latitude_input)
    EditText walletLatitudeInput;
    @BindView(R.id.wallet_longitude_input)
    EditText walletLongitudeInput;
    @BindView(R.id.car_latitude_layout)
    TextInputLayout carLatitudeLayout;
    @BindView(R.id.car_longitude_layout)
    TextInputLayout carLongitudeLayout;
    @BindView(R.id.car_latitude_input)
    EditText carLatitudeInput;
    @BindView(R.id.car_longitude_input)
    EditText carLongitudeInput;
    @BindView(R.id.set_car_position_button)
    TextView setCarPositionButton;
    @BindView(R.id.set_wallet_position_button)
    TextView setWalletPositionButton;
    @BindView(R.id.start_monitor_button)
    TextView monitorButton;
    @BindView(R.id.no_permission_overlay)
    View overlay;
    @BindView(R.id.save_button)
    TextView saveButton;

    public static final int LOCATION_PERMISSIONS = 99;
    public static final String LOCATION_REGEX = "(\\d|\\d\\d|\\d\\d\\d)[.][0-9]+";

    private boolean flag = false;
    private GPSTracker gpsTracker;
    private View view;
    private Wallet wallet = new Wallet();
    private ObjectOfInterest car = new ObjectOfInterest();
    private DataBaseAdapter dataBaseAdapter;

    public static PositionsFragment getInstance() {
        return new PositionsFragment();
    }

    @Override
    public String getFragmentTag() {
        return this.getClass().getName();
    }

    @Override
    protected int onFragmentContentView() {
        return R.layout.positions_fragment;
    }

    @Override
    protected void onCreateFragmentView(View v, ViewGroup container, Bundle savedInstanceState) {
        view = v;
        dataBaseAdapter = new DataBaseAdapter(getContext());
        dataBaseAdapter.open();
    }

    @Override
    protected void onViewsFragment(View view, Bundle savedInstanceState) {
        askForPermissions();
        Cursor cursor = dataBaseAdapter.getAllItems();
        if (cursor != null && cursor.getCount() > 0) {
            loadOldLocations(cursor);
        }
    }

    public void loadOldLocations(Cursor cursor) {
        cursor.moveToFirst();
        walletLatitudeInput.setText(String.valueOf(cursor.getDouble(DataBaseAdapter.LATITUDE_COLUMN)));
        walletLongitudeInput.setText(String.valueOf(cursor.getDouble(DataBaseAdapter.LONGITUDE_COLUMN)));
        wallet.setLongitude(Double.valueOf(walletLongitudeInput.getText().toString()));
        wallet.setLatitude(Double.valueOf(walletLatitudeInput.getText().toString()));
        cursor.moveToNext();
        carLatitudeInput.setText(String.valueOf(cursor.getDouble(DataBaseAdapter.LATITUDE_COLUMN)));
        carLongitudeInput.setText(String.valueOf(cursor.getDouble(DataBaseAdapter.LONGITUDE_COLUMN)));
        car.setLongitude(Double.valueOf(carLongitudeInput.getText().toString()));
        car.setLatitude(Double.valueOf(carLatitudeInput.getText().toString()));

    }

    public void askForPermissions() {
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // No explanation needed, we can request the permission.

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    LOCATION_PERMISSIONS);

            // LOCATION_PERMISSIONS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    overlay.setVisibility(View.VISIBLE);
                    blockIfNoPermission();
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @OnClick(R.id.save_button)
    public void saveData() {
        if (checkIfFieldMatches()) {
            dataBaseAdapter.updateInstertLocation(
                    "wallet", Double.valueOf(walletLatitudeInput.getText().toString()),
                    Double.valueOf(walletLongitudeInput.getText().toString()));
            dataBaseAdapter.updateInstertLocation(
                    "car", Double.valueOf(carLatitudeInput.getText().toString()),
                    Double.valueOf(carLongitudeInput.getText().toString()));
            Snackbar.make(view, R.string.snackbar_data_saved, Snackbar.LENGTH_SHORT).show();
        } else {
            Snackbar.make(view, R.string.snackbar_fill_blanks, Snackbar.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.wallet_checkbox)
    public void toggleWallet() {
        walletLatitudeLayout.setEnabled(!walletCheckbox.isChecked());
        walletLongitudeLayout.setEnabled(!walletCheckbox.isChecked());
        wallet.setWithUser(walletCheckbox.isChecked());
        setWalletPositionButton.setEnabled(!walletCheckbox.isChecked());
    }

    @OnClick(R.id.set_wallet_position_button)
    public void setCurrentPositionAsWalletPosition() {
        gpsTracker = new GPSTracker(getContext());
        if (gpsTracker.canGetLocation()) {
            walletLatitudeInput.setText(String.valueOf(gpsTracker.getLatitude()));
            walletLongitudeInput.setText(String.valueOf(gpsTracker.getLongitude()));
            wallet.setLatLong(gpsTracker.getCurrentLocation());

        }
        gpsTracker.stopUsingGPS();
    }

    @OnClick(R.id.set_car_position_button)
    public void setCurrentPositionAsCarPosition() {
        gpsTracker = new GPSTracker(getContext());
        if (gpsTracker.canGetLocation()) {
            carLatitudeInput.setText(String.valueOf(gpsTracker.getLatitude()));
            carLongitudeInput.setText(String.valueOf(gpsTracker.getLongitude()));
            car.setLatLong(gpsTracker.getCurrentLocation());
        }
        gpsTracker.stopUsingGPS();
    }

    @OnClick(R.id.start_monitor_button)
    public void startMonitoring() {
        if (checkIfFieldMatches()) {
            setAllStates(flag);
            if (!flag) {
                car.setLongitude(Double.valueOf(carLongitudeInput.getText().toString()));
                car.setLatitude(Double.valueOf(carLatitudeInput.getText().toString()));
                wallet.setLongitude(Double.valueOf(walletLongitudeInput.getText().toString()));
                wallet.setLatitude(Double.valueOf(walletLatitudeInput.getText().toString()));
                gpsTracker = new GPSTracker(getContext(), wallet, car);
                monitorButton.setText(getString(R.string.stop_tracking));
                flag = !flag;
            } else {
                gpsTracker.stopUsingGPS();
                monitorButton.setText(getString(R.string.start_tracking));
                flag = !flag;
            }
        } else {
            Snackbar.make(view, R.string.snackbar_fill_blanks, Snackbar.LENGTH_SHORT).show();
        }
    }

    public void blockIfNoPermission() {
        monitorButton.setEnabled(false);
        saveButton.setEnabled(false);
        walletLatitudeInput.setEnabled(false);
        walletLongitudeInput.setEnabled(false);
        walletCheckbox.setEnabled(false);
        carLatitudeInput.setEnabled(false);
        carLongitudeInput.setEnabled(false);
        setCarPositionButton.setEnabled(false);
        setWalletPositionButton.setEnabled(false);
    }

    public void setAllStates(boolean shouldBeEnabled) {
        if (!walletCheckbox.isChecked()) {
            walletLatitudeInput.setEnabled(shouldBeEnabled);
            walletLongitudeInput.setEnabled(shouldBeEnabled);
        }
        saveButton.setEnabled(shouldBeEnabled);
        walletCheckbox.setEnabled(shouldBeEnabled);
        carLatitudeInput.setEnabled(shouldBeEnabled);
        carLongitudeInput.setEnabled(shouldBeEnabled);
        setCarPositionButton.setEnabled(shouldBeEnabled);
        setWalletPositionButton.setEnabled(shouldBeEnabled);
    }

    public boolean checkIfFieldMatches() {
        boolean inputFieldsFlag = true;
        if (!walletLatitudeInput.getText().toString().matches(LOCATION_REGEX))
            inputFieldsFlag = false;
        if (!walletLongitudeInput.getText().toString().matches(LOCATION_REGEX))
            inputFieldsFlag = false;
        if (!carLatitudeInput.getText().toString().matches(LOCATION_REGEX)) inputFieldsFlag = false;
        if (!carLongitudeInput.getText().toString().matches(LOCATION_REGEX))
            inputFieldsFlag = false;

        return inputFieldsFlag;
    }

    @Override
    public void onDestroyView() {
        dataBaseAdapter.close();
        super.onDestroyView();
    }
}
