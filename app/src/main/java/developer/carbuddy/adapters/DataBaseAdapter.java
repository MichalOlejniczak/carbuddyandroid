package developer.carbuddy.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseAdapter {
    public static final String DATABASE_NAME = "Locations.db";
    public static final String LOCATION_TABLE = "latitude_longitude";
    public static final String KEY_ID = "id";
    public static final String ID_OPTIONS = "INTEGER PRIMARY KEY AUTOINCREMENT";
    public static final int ID_COLUMN = 0;
    public static final String KEY_NAME = "item_name";
    public static final int NAME_COLUMN = 1;
    public static final String KEY_LATITUDE = "latitude";
    public static final int LATITUDE_COLUMN = 2;
    public static final String KEY_LONGITUDE = "longitude";
    public static final int LONGITUDE_COLUMN = 3;
    private static final String DB_CREATE_SHOP_TABLE =
            "CREATE TABLE " + LOCATION_TABLE +
                    "( " + KEY_ID + " " + ID_OPTIONS +
                    ", " + KEY_NAME +
                    ", " + KEY_LATITUDE + " " +
                    ", " + KEY_LONGITUDE +
                    ");";
    private static final String DROP_SHOP_TABLE =
            "DROP TABLE IF EXISTS " + LOCATION_TABLE;

    private DataBaseHelper dbHelper;
    private SQLiteDatabase db;
    private Context context;
    private boolean isAlreadyCreated = false;

    public DataBaseAdapter(Context context) {
        this.context = context;
    }

    public DataBaseAdapter open() {
        dbHelper = new DataBaseHelper(context, DATABASE_NAME, null, 1);
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLException e) {
            db = dbHelper.getReadableDatabase();
        }
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public long saveLocation(String name, double latitude, double longitude) {
        ContentValues newListValues = new ContentValues();
        newListValues.put(KEY_NAME, name);
        newListValues.put(KEY_LATITUDE, latitude);
        newListValues.put(KEY_LONGITUDE, longitude);
        return db.insert(LOCATION_TABLE, null, newListValues);
    }


    public void updateLocation(String name, double latitude, double longitude) {
        String query = "UPDATE " + LOCATION_TABLE
                + " SET " + KEY_LATITUDE + " ='" + latitude + "', "
                + KEY_LONGITUDE + " ='" + longitude + "'"
                + " WHERE " + KEY_NAME + " ='" + name + "';";
        db.execSQL(query);
    }

    public void updateInstertLocation(String name, double latitude, double longitude) {
        if (isExist(name)) {
            updateLocation(name, latitude, longitude);
        } else {
            saveLocation(name, latitude, longitude);
        }
    }

    public boolean isExist(String name) {
        String selectQuery = "SELECT  *  FROM " + LOCATION_TABLE + " WHERE " + KEY_NAME + " ='" + name + "';";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() > 0) {
            isAlreadyCreated = true;
            cursor.close();
        }

        return isAlreadyCreated;
    }

    public Cursor getAllItems() {
        String[] col = {KEY_ID, KEY_NAME, KEY_LATITUDE, KEY_LONGITUDE};
        return db.query(LOCATION_TABLE, col, null, null, null, null, null);
    }

    public boolean removeItem(long id) {
        String where = KEY_ID + "=" + id;
        return db.delete(LOCATION_TABLE, where, null) > 0;
    }

    public static class DataBaseHelper extends SQLiteOpenHelper {
        public DataBaseHelper(Context context, String name,
                              SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE_SHOP_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DROP_SHOP_TABLE);
            onCreate(db);
        }
    }
}
